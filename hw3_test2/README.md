Removed troublesome boost dependencies in cmake

## build instructions:
cmake .
make
./hw2

Key press to switch modes:
'1': orthogonal
'2': perspective
'3': first person

Controls in modes 1 and 2:
Use the arrow keys to rotate the scene.


Controls in mode 3: (First Person)
Movement is restricted to the plane in which y = 1.
W: Move Forward
A: Rotate Left
D: Move Backward
S: Rotate Right

r: reset parameters



t: toggle the coordinate axes


This Assignment took me approximately 20 hours to complete



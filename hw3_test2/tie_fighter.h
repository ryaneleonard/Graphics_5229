//
// Created by ryan on 6/16/18.
//

#ifndef HW2_TIE_FIGHTER_H
#define HW2_TIE_FIGHTER_H
#include "CSCIx229.h"
void cockpit_spheres(double x,double y,double z,double r,double color_r, double color_g, double color_b, unsigned int texture);

void draw_cylinder(double top_radius, double bottom_radius, double height, double color_r, double color_g, double color_b, bool hex, unsigned int texture);

void tie_fighter(double x, double y, double z, double scale, unsigned int cockpit_texture, unsigned int wing_texture);

void coin(double x,double y,double z,double r,double d, unsigned int texture);

void frustum(unsigned int texture);

void connecting_cylinder(unsigned int texture);

void tie_wing_prism(double height, double x, double y, double z, double theta, unsigned int texture);

#endif //HW2_TIE_FIGHTER_H

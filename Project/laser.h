//
// Created by ryan on 6/28/18.
//

#ifndef PATENT_INFRINGEMENT_TIMEBOMB_LASER_H
#define PATENT_INFRINGEMENT_TIMEBOMB_LASER_H
#include <GL/glut.h>
#include <glm/vec3.hpp>
#include <iostream>
#include <math.h>
#include "AABB.h"

class Laser {
public:
    Laser(double PosX, double PosY, double PosZ, double Dx, double Dy, double Dz, double th, double ph) :
            PosX_(PosX),
            PosY_(PosY),
            PosZ_(PosZ),
            Dx_(Dx),
            Dy_(Dy),
            Dz_(Dz),
            th_(th),
            ph_(ph)
    {
//        this->bbox.initialize(this->PosX_ - this->Dx_ * .5, this->PosY_ - this->Dy_ * .5, this->PosZ_ - this->Dz_ * .5, 0.02, 0.02, 0.02);
        this->bbox.initialize(this->PosX_, this->PosY_ , this->PosZ_, 0.02, 0.02, 0.02);
    }

    ~Laser(){}
    // At each call to timer, call the update function on each laser object. This will update the age of the laser and its position
    void update(double time = 50);
    // If the current age is greater than the max age, this returns false
    bool should_exist();

    // Draw a green cylinder at the appropriate location
    void draw_laser();

    AABB bbox;

private:
    // The direction of travel
    double Dx_, Dy_, Dz_;

    // The current position
    double PosX_, PosY_, PosZ_;

    // Age of laser
    double age_ = 0;

    // The age at which a laser should terminate
    double max_age_ = 10000; // Start off with 5 seconds

    // The rotation about the y axis
    double th_;
    // The rotation about the y axis
    double ph_;
};


#endif //PATENT_INFRINGEMENT_TIMEBOMB_LASER_H

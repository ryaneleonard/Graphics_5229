//
// Created by ryan on 7/3/18.
//

#include "ObjectModel.h"

void ObjectModel::draw_model() {
//  Erase the window and the depth buffer
//    glEnable(GL_DEPTH_TEST);

    glColor3f(1,1,1);

    glEnable(GL_NORMALIZE);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->texture1_);

    glPushMatrix();
    glTranslated(this->x_, this->y_, this->z_);
    glRotated(this->ph_, 0, 1, 0);
    glScaled(this->scale_, this->scale_, this->scale_);

    glCallList(this->object_);

    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    ErrCheck("ObjectModel");

}

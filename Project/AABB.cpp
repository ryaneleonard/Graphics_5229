//
// Created by ryan on 7/2/18.
//

#include "AABB.h"

double AABB::min_x(){
    return this->bbox_center[0] - this->bbox_radius[0];
}

double AABB::max_x(){
    return this->bbox_center[0] + this->bbox_radius[0];
}

double AABB::min_y(){
    return this->bbox_center[1] - this->bbox_radius[1];
}

double AABB::max_y(){
    return this->bbox_center[1] + this->bbox_radius[1];
}

double AABB::min_z(){
    return this->bbox_center[2] - this->bbox_radius[2];
}

double AABB::max_z(){
    return this->bbox_center[2] + this->bbox_radius[2];
}


bool AABB::isInside(double x, double y, double z, AABB &some_object){
    // Check if a specific point lies within the AABB of another object
    return (
            ((x >= some_object.min_x() && x <= some_object.max_x()) &&
           (y >= some_object.min_y() && y <= some_object.max_y()) &&
           (z >= some_object.min_z() && z <= some_object.max_z()))
    );
}

bool AABB::isCollided(AABB &some_object) {
    if (isInside(this->min_x(), min_y(), min_z(), some_object))
        return true;
    else if (isInside(this->max_x(), min_y(), min_z(), some_object))
        return true;
    else if (isInside(this->min_x(), max_y(), min_z(), some_object))
        return true;
    else if (isInside(this->max_x(), max_y(), min_z(), some_object))
        return true;
    else if (isInside(this->min_x(), min_y(), max_z(), some_object))
        return true;
    else if (isInside(this->max_x(), min_y(), max_z(), some_object))
        return true;
    else if (isInside(this->min_x(), max_y(), max_z(), some_object))
        return true;
    else if (isInside(this->max_x(), max_y(), max_z(), some_object))
        return true;
    else return false;
}
import csv
import sys

if __name__ == "__main__":
    reversed_list = []
    input_file = sys.argv[1]
    with open(input_file) as r:
        reader = csv.reader(r)
        for row in reader:
            values = row[0].split(' ')
            print("Before:")
            print(values)
            if values[0] == 'vn':
                values = [values[0]] + [str(-float(val)) for val in values[1:]]
            reversed_list.append(values)
            print("After: ")
            print(values)

    tmp_file  = input_file.split('/')
    output_file = "/".join(tmp_file[:-1]) + "/reversed_normals_" +tmp_file[-1]
    with open(output_file, 'w') as w:
        writer = csv.writer(w, delimiter=' ')
        for row in reversed_list:
            writer.writerow(row)

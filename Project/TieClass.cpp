//
// Created by ryan on 7/1/18.
//

#include "TieClass.h"

void TieClass::Vertex(double th,double ph)
{
    double x = Sin(th)*Cos(ph);
    double y = Cos(th)*Cos(ph);
    double z =         Sin(ph);
    //  For a sphere at the origin, the position
    //  and normal vectors are the same
    glNormal3d(x,y,z);
    glTexCoord2d(th/360.0,ph/180.0+0.5);
    glVertex3d(x,y,z);

}


void TieClass::tie_wing_prism(double height, double x, double y, double z, double theta, unsigned int texture){
    // Draw a square for each end.
    double end_dimension = 0.05;
    double tie_grey = 0.6;
    glPushMatrix();
    glTranslated(x, y, z);
    glRotated(theta, 0, 0, 1);
    glScaled(1, 1, 1);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);

    glBegin(GL_QUADS);
    // Top:
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(0, 1, 0);
    glTexCoord2f(0, 0);
    glVertex3f(end_dimension, height/2, end_dimension);
    glTexCoord2f(1, 0);
    glVertex3f(-end_dimension, height/2, end_dimension);
    glTexCoord2f(0, 1);
    glVertex3f(-end_dimension, height/2, -end_dimension);
    glTexCoord2f(1, 1);
    glVertex3f(end_dimension, height/2, -end_dimension);

    // Bottom:
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(0, -1, 0);
    glTexCoord2f(0, 0);
    glVertex3f(end_dimension, -height/2, end_dimension);
    glTexCoord2f(1, 0);
    glVertex3f(-end_dimension, -height/2, end_dimension);
    glTexCoord2f(0, 1);
    glVertex3f(-end_dimension, -height/2, -end_dimension);
    glTexCoord2f(1, 1);
    glVertex3f(end_dimension, -height/2, -end_dimension);

    //Front
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(0, 0, 1);
    glTexCoord2f(0, 0);
    glVertex3f(end_dimension, -height/2, end_dimension); // bottom right
    glTexCoord2f(1, 0);
    glVertex3f(-end_dimension, -height/2, end_dimension); // bottom left
    glTexCoord2f(0, 1);
    glVertex3f(-end_dimension, height/2, end_dimension); // top left
    glTexCoord2f(1, 1);
    glVertex3f(end_dimension, height/2, end_dimension); // top right

    // Back
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(0, 0, -1);
    glTexCoord2f(0, 0);
    glVertex3f(end_dimension, -height/2, -end_dimension); // bottom right
    glTexCoord2f(1, 0);
    glVertex3f(-end_dimension, -height/2, -end_dimension); // bottom left
    glTexCoord2f(0, 1);
    glVertex3f(-end_dimension, height/2, -end_dimension); // top left
    glTexCoord2f(1, 1);
    glVertex3f(end_dimension, height/2, -end_dimension); // top right

    // Right
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(1, 0, 0);
    glTexCoord2f(0, 0);
    glVertex3f(end_dimension, -height/2, end_dimension); // bottom right
    glTexCoord2f(1, 0);
    glVertex3f(end_dimension, -height/2, -end_dimension); // bottom left
    glTexCoord2f(0, 1);
    glVertex3f(end_dimension, height/2, -end_dimension); // top left
    glTexCoord2f(1, 1);
    glVertex3f(end_dimension, height/2, end_dimension); // top right

    // Left
    glColor3f(tie_grey, tie_grey, tie_grey);
    glNormal3f(-1, 0, 0);
    glTexCoord2f(0, 0);
    glVertex3f(-end_dimension, -height/2, end_dimension); // bottom right
    glTexCoord2f(1, 0);
    glVertex3f(-end_dimension, -height/2, -end_dimension); // bottom left
    glTexCoord2f(0, 1);
    glVertex3f(-end_dimension, height/2, -end_dimension); // top left
    glTexCoord2f(1, 1);
    glVertex3f(-end_dimension, height/2, end_dimension); // top right
    glEnd();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);

}

void TieClass::tie_wing(unsigned int cockpit_texture, unsigned int wing_texture){
    // Draw a stretched black hexagon
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, wing_texture);
    glBegin(GL_TRIANGLE_FAN);

    glColor3f(.2, .2, .2);
    glNormal3f(0, 0, -1);

    glTexCoord2f(0.5,0.5);
    glVertex3f(0, 0, 0); // center

    glTexCoord2f(1, 1);
    glVertex3f(1.4, 0, 0); // right

    glTexCoord2f(1, 0.5);
    glVertex3f(1, 2, 0); // top right

    glTexCoord2f(0, 1);
    glVertex3f(-1, 2, 0); // top left

    glTexCoord2f(0, 0);
    glVertex3f(-1.4, 0, 0); // left

    glTexCoord2f(0, 0.5);
    glVertex3f(-1, -2, 0); // bottom left

    glTexCoord2f(1, 0);
    glVertex3f(1, -2, 0); // bottom right

    glTexCoord2f(1, 1);
    glVertex3f(1.4, 0, 0); // Redefine the final point (right)
    glEnd(); // GL_TRIANGLE_FAN
//    glDisable(GL_TEXTURE_2D);

    ///////////////////////////////
    glBindTexture(GL_TEXTURE_2D, wing_texture);
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(.2, .2, .2);
    glNormal3f(0, 0, 1);

    glTexCoord2f(0.5,0.5);
    glVertex3f(0, 0, .001); // center

    glTexCoord2f(1, 1);
    glVertex3f(1.4, 0, .001); // right

    glTexCoord2f(1, 0.5);
    glVertex3f(1, -2, .001); // bottom right

    glTexCoord2f(0, 1);
    glVertex3f(-1, -2, .001); // bottom left

    glTexCoord2f(0, 0);
    glVertex3f(-1.4, 0, .001); // left

    glTexCoord2f(0, 0.5);
    glVertex3f(-1, 2, .001); // top left

    glTexCoord2f(1, 0);
    glVertex3f(1, 2, .001); // top right

    glTexCoord2f(1, 1);
    glVertex3f(1.4, 0, .001); // right

//    glTexCoord2f(0.5, 0.5);
//    glVertex3f(1.4, 0, .001); // Redefine the final point
//    glVertex3f(1.4, 0, .001); // Redefine the final point
    glEnd();


    // Draw grey prisms around the outer edges of the hexagon
    // Prisms radiating outwards
    tie_wing_prism(2.8, 0, 0, 0, 90, cockpit_texture);
    tie_wing_prism(4.47, 0, 0, 0, 90 - 63.43, cockpit_texture);
    tie_wing_prism(4.47, 0, 0, 0, -(90 - 63.43), cockpit_texture);


    // Top
    tie_wing_prism(2.08, 0, 2, 0, 90, cockpit_texture);
    // Bottom
    tie_wing_prism(2.08, 0, -2, 0, 90, cockpit_texture);
    tie_wing_prism(2.05, 1.2, 1, 0, 11.304, cockpit_texture);
    tie_wing_prism(2.05, 1.2, -1, 0, -11.304, cockpit_texture);
    tie_wing_prism(2.05, -1.2, -1, 0, 11.304, cockpit_texture);
    tie_wing_prism(2.05, -1.2, 1, 0, -11.304, cockpit_texture);
}


void TieClass::frustum(unsigned int texture)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    const double PI = 3.14159;

    /* top triangle */
    double i, resolution  = 0.1;
    double height = 1;
    double top_radius = 0.7;
    double bottom_radius = 0.9;

    glPushMatrix();
    glTranslatef(0, 0, 0);
// top cap
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, 1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, height/2);  /* center */
    for (i = 4 * PI; i >= 0; i -= resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(bottom_radius * cos(i), bottom_radius * sin(i), height/2);
    }
    /* close the loop back to 0 degrees */
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(bottom_radius, 0, height/2);
    glEnd();

    // bottom cap
    /* bottom triangle: note: for is in reverse order */
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, -1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, -height/2);  /* center */
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(top_radius * cos(i), top_radius * sin(i), -height/2);
    }
    glEnd();

    /* middle tube */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        const float tc = ( i / (float)( 2 * PI ) );
        glTexCoord2f( tc, 0.0 );
        glVertex3f(top_radius * cos(i), top_radius * sin(i), -height/2);
        glNormal3f(top_radius * cos(i), top_radius * sin(i), 0.2);
        glTexCoord2f( tc, 1.0 );
        glVertex3f(bottom_radius * cos(i), bottom_radius * sin(i), height/2);
        glNormal3f(bottom_radius * cos(i), bottom_radius * sin(i), 0.2);
    }
    /* close the loop back to zero degrees */
    glTexCoord2f( 0.0, 0.0 );
    glVertex3f(top_radius, 0, -height/2);
    glTexCoord2f( 0.0, 1.0 );
    glVertex3f(bottom_radius, 0, height/2);
    glEnd();



    glBegin(GL_TRIANGLE_FAN);
    glColor3f(0, 0, 0);
    glVertex3f(0, 0, -height/2 - 0.01);
    for (double angle = 0; angle <=360; angle += 60){
        double hex_x = Cos(angle) * top_radius / 2.5;
        double hex_y = Sin(angle) * top_radius / 2.5;
        glColor3f(0, 0, 0);
        glVertex3f(hex_x, hex_y, -height/2 -0.01);
    }
    glEnd();

    // Draw port hole and engines
//        glDisable(GL_POLYGON_OFFSET_FILL);

    // Left Engine
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1, 0, 0);
    glNormal3f(0, 0, -1);

    glVertex3f(-0.45, 0, -height/2-.01);
    for (double angle = 0.0; angle <= 360; angle += 1){
        double x_bottom = Cos(angle) * .05 - .45;
        double y_bottom = Sin(angle) * .05;
        glColor3f(1, 0, 0);
        glVertex3f(x_bottom, y_bottom, -height/2);
    }
    glEnd();

    // Right Engine
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1, 0, 0);
    glNormal3f(0, 0, -1);
    glVertex3f(.45, 0, -height/2 - .01);
    for (double angle = 0.0; angle <= 360; angle += 1){
        double x_bottom = Cos(angle) * .05 + .45;
        double y_bottom = Sin(angle) * .05;
        glColor3f(1, 0, 0);
        glVertex3f(x_bottom, y_bottom, -height/2 - .01);
    }
    glEnd();



    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}

void TieClass::connecting_cylinder(unsigned int texture)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    const double PI = 3.14159;

    /* top triangle */
    double i, resolution  = 0.1;
    double height = 5;
    double top_radius = 0.5;
    double bottom_radius = 0.5;

    glPushMatrix();
//    glTranslatef(0, , 0);
// top cap
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, 1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0,0,  height/2);  /* center */
    for (i = 4 * PI; i >= 0; i -= resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(bottom_radius * cos(i),  bottom_radius * sin(i), height/2);
    }
    /* close the loop back to 0 degrees */
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(bottom_radius, 0, height/2);
    glEnd();

    // bottom cap
    /* bottom triangle: note: for is in reverse order */
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, -1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, -height/2);  /* center */
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(top_radius * cos(i),  top_radius * sin(i), -height/2);
    }
    glEnd();

    /* middle tube */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        const float tc = ( i / (float)( 2 * PI ) );
        glTexCoord2f( tc, 0.0 );
        glVertex3f(top_radius * cos(i), top_radius * sin(i), height/2);
        glNormal3f(top_radius * cos(i), top_radius * sin(i), 0);
        glTexCoord2f( tc, 1.0 );
        glVertex3f(bottom_radius * cos(i), bottom_radius * sin(i), -height/2);
        glNormal3f(bottom_radius * cos(i), bottom_radius * sin(i), 0);

    }
    /* close the loop back to zero degrees */
    glTexCoord2f( 0.0, 0.0 );
    glVertex3f(top_radius,0, height/2);
    glTexCoord2f( 0.0, 1.0 );
    glVertex3f(bottom_radius,0,  -height/2);
    glEnd();

    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}


/*
 *  Modified code from spheres2
 */
void TieClass::cockpit_spheres(double x,double y,double z,double r,double color_r, double color_g, double color_b, unsigned int texture)
{
    const int d=5;
    int th,ph;
    //  Save transformation
    glPushMatrix();
    //  Offset and scale
    glTranslated(x,y,z);
    glScaled(r,r,r);
    //  Latitude bands
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor3f(color_r, color_g, color_b);
    for (ph=-90;ph<90;ph+=d)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=d)
        {
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph);
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph+d);
        }
        glEnd();
    }
    //  Undo transformations
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}



void TieClass::tie_fighter(/*double x, double y, double z, double roll, double pitch, double yaw, double scale, unsigned int cockpit_texture, unsigned int wing_texture*/){

    glPushMatrix(); // 1
    glTranslated(this->pos_x_, this->pos_y_, this->pos_z_);
    glRotated( -this->yaw_ -90, 0, 1, 0);
    glRotated(this->roll_, 0, 0, 1);
    glRotated(this->pitch_, 1, 0, 0);
    glScaled(this->scale_, this->scale_, this->scale_);
    // Draw a grey sphere with a black circular window and a black truncated cone and hexagonal exhaust
    cockpit_spheres(0, 0, 0, 1, 0.6, 0.6, 0.6, this->cockpit_texture_);
    cockpit_spheres(0.14, 0, 0, 0.9, 0, 0, 0, this->cockpit_texture_); // Black window

    glColor3f(0.6, 0.6, 0.6);

    connecting_cylinder(this->cockpit_texture_);
//    draw_cylinder(.5, .5, 5, 0.6, 0.6, 0.6, false, cockpit_texture);
    // Draw a frustum coming out of the cockpit

    glPushMatrix(); // 2
    glTranslated(-0.5, 0, 0);
    glRotated(90, 0, 1, 0);
    frustum(this->cockpit_texture_);

    glPopMatrix();// 2

    glPushMatrix(); //3
    glTranslated(0, 0, 2.2);
    glScaled(1.5, 1.5, 1.5);

    tie_wing(this->cockpit_texture_, this->wing_texture_);
    glPopMatrix(); //3

    glPushMatrix(); // 4
    glTranslated(0, 0, -2.2);
    glScaled(1.5, 1.5, 1.5);
    tie_wing(this->cockpit_texture_, this->wing_texture_);
    glPopMatrix(); // 4
    glPopMatrix(); // 1
}

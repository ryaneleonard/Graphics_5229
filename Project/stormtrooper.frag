//  Set the fragment color

uniform sampler2D tex;

void main()
{
    vec4 color = gl_Color * texture2D(tex, gl_TexCoord[0].xy);
    if (color[0] > .2 && color[1] < 0.1 && color[2] < 0.1)
        discard;
    else
        gl_FragColor = gl_Color * texture2D(tex,gl_TexCoord[0].xy);



}




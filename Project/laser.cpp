//
// Created by ryan on 6/28/18.
//

#include "laser.h"


void Laser::update(double time) {
    double speed = 1;
    this->PosX_ +=  speed * this->Dx_;
    this->PosY_ += speed * this->Dy_;
    this->PosZ_ += speed * this->Dz_;
    this->age_ += time;
    this->bbox.update_center(this->PosX_, this->PosY_, this->PosZ_);
    if (this->age_ > this->max_age_/1000)
        glDisable(GL_LIGHT1);
}


// If the current age of the laser exceeds the maximum allowed lifetime, send a message to pop it from the vector
bool Laser::should_exist() {
    return this->age_ > this->max_age_;
}

void Laser::draw_laser(){
    // Lighting Stuff
    glShadeModel(GL_SMOOTH);
    float Ambient[] = {0.001 * 0, 0.001 * 0, 0.001 * 0, 1.0};
    float Diffuse[] = {0.001 * 1, 0.001 * 1, 0.001 * 1, 1.0};
    float Specular[] = {0.001 * 1, 0.1 * 1, 0.01 * 1, 1.0};

    // End Lighting Stuff


    glColor3f(0, 1, 0);
    const double PI = 3.14159;

    /* top triangle */
    double i, resolution  = 0.1;
    double height = 2;
    double top_radius = 0.02;
    double bottom_radius = 0.02;


    // Draw laser relative to firing path
    glPushMatrix();
    glTranslated(this->PosX_, this->PosY_, this->PosZ_);
    glRotated(-this->th_, 0, 1, 0);
    glRotated(-this->ph_, 1, 0, 0);

    // Draw laser relative to the origin
    glPushMatrix();
    // top cap
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, 1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0,0,  height/2);  /* center */
    for (i = 4 * PI; i >= 0; i -= resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(bottom_radius * cos(i),  bottom_radius * sin(i), height/2);
    }
    /* close the loop back to 0 degrees */
//    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(bottom_radius, 0, height/2);
    glEnd();

    // bottom cap
    /* bottom triangle: note: for is in reverse order */
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, -1);
//    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, -height/2);  /* center */
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(top_radius * cos(i),  top_radius * sin(i), -height/2);
    }
    glEnd();

    /* middle tube */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        const float tc = ( i / (float)( 2 * PI ) );
        glTexCoord2f( tc, 0.0 );
        glVertex3f(top_radius * cos(i), top_radius * sin(i), height/2);
        glNormal3f(top_radius * cos(i), top_radius * sin(i), 0);
        glTexCoord2f( tc, 1.0 );
        glVertex3f(bottom_radius * cos(i), bottom_radius * sin(i), -height/2);
        glNormal3f(bottom_radius * cos(i), bottom_radius * sin(i), 0);
    }
    /* close the loop back to zero degrees */
    glTexCoord2f( 0.0, 0.0 );
    glVertex3f(top_radius,0, height/2);
    glTexCoord2f( 0.0, 1.0 );
    glVertex3f(bottom_radius,0,  -height/2);
    glEnd();
    glPopMatrix(); // End drawing relative to origin

    glPopMatrix(); // End drawing relative to firing position

    // Lighting Stuff
    //  OpenGL should normalize normal vectors
    glEnable(GL_NORMALIZE);
    //  Enable lighting
    glEnable(GL_LIGHTING);
    //  Location of viewer for specular calculations
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
    //  glColor sets ambient and diffuse color materials
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    //  Enable light 1
    glEnable(GL_LIGHT1);
    //  Set ambient, diffuse, specular components and position of light 0
    float Position[4] = {this->PosX_, this->PosY_, this->PosZ_, 1};
    glLightfv(GL_LIGHT1, GL_AMBIENT, Ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, Diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, Specular);
    glLightfv(GL_LIGHT1, GL_POSITION, Position);
    // End Lighting Stuff
    glutPostRedisplay();
}
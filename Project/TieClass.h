//
// Created by ryan on 7/1/18.
//

#ifndef COPYRIGHT_INFRINGEMENT_TIMEBOMB_TIECLASS_H
#define COPYRIGHT_INFRINGEMENT_TIMEBOMB_TIECLASS_H

#include <GL/glut.h>
#include <math.h>
#include "AABB.h"
//  Cosine and Sine in degrees
#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))

class TieClass {
public:
    // Constructor
    TieClass(double x, double y, double z, double roll, double pitch, double yaw, double scale, unsigned int cockpit_texture, unsigned int wing_texture):
    pos_x_(x), pos_y_(y), pos_z_(z), roll_(roll), pitch_(pitch), yaw_(yaw), scale_(scale), cockpit_texture_(cockpit_texture), wing_texture_(wing_texture){
        this->bbox.initialize(this->pos_x_, this->pos_y_, this->pos_z_, scale * 7, scale * 7, scale * 7);
    }
    // Destructor
    ~TieClass(){}

    void tie_fighter(/*double x, double y, double z, double roll, double pitch, double yaw, double scale, unsigned int cockpit_texture, unsigned int wing_texture*/);
    void death_star(double x, double y, double z, double scale, unsigned int texture);

    AABB bbox;
private:
    // Position, scale and orientation variables.
    double pos_x_, pos_y_, pos_z_, roll_, pitch_, yaw_, scale_;
    unsigned int cockpit_texture_, wing_texture_;

    void Vertex(double th,double ph);

    void tie_wing_prism(double height, double x, double y, double z, double theta, unsigned int texture);

    void tie_wing(unsigned int cockpit_texture, unsigned int wing_texture);

    void frustum(unsigned int texture);

    void connecting_cylinder(unsigned int texture);

    void cockpit_spheres(double x,double y,double z,double r,double color_r, double color_g, double color_b, unsigned int texture);
};

#endif //COPYRIGHT_INFRINGEMENT_TIMEBOMB_TIECLASS_H

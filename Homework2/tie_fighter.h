//
// Created by ryan on 6/16/18.
//

#ifndef HW2_TIE_FIGHTER_H
#define HW2_TIE_FIGHTER_H

void tie_fighter(double x, double y, double z, double scale);

void draw_cylinder(double top_radius, double bottom_radius, double height, double color_r, double color_g, double color_b, bool hex=false);

#endif //HW2_TIE_FIGHTER_H

//
// Created by ryan on 6/16/18.
//

// Requirements:
// [] Draw a tie fighter
// [] Toggle between orthogonal and perspective projections
// [] Draw same object with different scale and position
// [] First Person perspective



/*
*  3D Objects
*
*  Demonstrates how to draw objects in 3D.
*
*  Key bindings:
*  m/M        Cycle through different sets of objects
*  a          Toggle axes
*  arrows     Change view angle
*  0          Reset view angle
*  ESC        Exit
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include "tie_fighter.h"
//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//int th=0;         //  Azimuth of view angle
//int ph=0;         //  Elevation of view angle
//int axes=1;       //  Display axes
//int mode=0;       //  What to display

//  Cosine and Sine in degrees
#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))

/*
 *  Convenience routine to output raster text
 *  Use VARARGS to make this more flexible
 */
//#define LEN 8192  //  Maximum length of text string
//void Print(const char* format , ...)
//{
//    char    buf[LEN];
//    char*   ch=buf;
//    va_list args;
//    //  Turn the parameters into a character string
//    va_start(args,format);
//    vsnprintf(buf,LEN,format,args);
//    va_end(args);
//    //  Display the characters one at a time at the current raster position
//    while (*ch)
//        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,*ch++);
//}

/*
 *  Draw vertex in polar coordinates
 */
static void Vertex(double th,double ph)
{
    glVertex3d(Sin(th)*Cos(ph) , Sin(ph) , Cos(th)*Cos(ph));
}


static void tie_wing_prism(double height, double x, double y, double z, double theta){
    // Draw a square for each end.
    double end_dimension = 0.05;
    double tie_grey = 0.6;
    glPushMatrix();
    glTranslated(x, y, z);
    glRotated(theta, 0, 0, 1);
    glScaled(1, 1, 1);
    glBegin(GL_QUADS);

    // Top:
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(end_dimension, height/2, end_dimension);
    glVertex3f(-end_dimension, height/2, end_dimension);
    glVertex3f(-end_dimension, height/2, -end_dimension);
    glVertex3f(end_dimension, height/2, -end_dimension);

    // Bottom:
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(end_dimension, -height/2, end_dimension);
    glVertex3f(-end_dimension, -height/2, end_dimension);
    glVertex3f(-end_dimension, -height/2, -end_dimension);
    glVertex3f(end_dimension, -height/2, -end_dimension);

    //Front
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(end_dimension, -height/2, end_dimension); // bottom right
    glVertex3f(-end_dimension, -height/2, end_dimension); // bottom left
    glVertex3f(-end_dimension, height/2, end_dimension); // top left
    glVertex3f(end_dimension, height/2, end_dimension); // top right

    // Back
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(end_dimension, -height/2, -end_dimension); // bottom right
    glVertex3f(-end_dimension, -height/2, -end_dimension); // bottom left
    glVertex3f(-end_dimension, height/2, -end_dimension); // top left
    glVertex3f(end_dimension, height/2, -end_dimension); // top right

    // Right
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(end_dimension, -height/2, end_dimension); // bottom right
    glVertex3f(end_dimension, -height/2, -end_dimension); // bottom left
    glVertex3f(end_dimension, height/2, -end_dimension); // top left
    glVertex3f(end_dimension, height/2, end_dimension); // top right

    // left
    glColor3f(tie_grey, tie_grey, tie_grey);
    glVertex3f(-end_dimension, -height/2, end_dimension); // bottom right
    glVertex3f(-end_dimension, -height/2, -end_dimension); // bottom left
    glVertex3f(-end_dimension, height/2, -end_dimension); // top left
    glVertex3f(-end_dimension, height/2, end_dimension); // top right
    glEnd();
    glPopMatrix();

}

static void tie_wing(){
    // Draw a stretched black hexagon
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(0, 0, 0);
    glVertex3f(0, 0, 0); // center
    glVertex3f(1.4, 0, 0); // right
    glVertex3f(1, 2, 0); // top right
    glVertex3f(-1, 2, 0); // top left
    glVertex3f(-1.4, 0, 0); // left
    glVertex3f(-1, -2, 0); // bottom left
    glVertex3f(1, -2, 0); // bottom right
    glVertex3f(1.4, 0, 0); // Redefine the final point
    glEnd(); // GL_TRIANGLE_FAN

    // Draw grey prisms around the outer edges of the hexagon
    tie_wing_prism(2.8, 0, 0, 0, 90);
    tie_wing_prism(4.47, 0, 0, 0, 90 - 63.43);
    tie_wing_prism(4.47, 0, 0, 0, -(90 - 63.43));

    tie_wing_prism(2, 0, 2, 0, 90);
    tie_wing_prism(2, 0, -2, 0, 90);
    tie_wing_prism(2.12, 1.2, 1, 0, 11.304);
    tie_wing_prism(2.12, 1.2, -1, 0, -11.304);
    tie_wing_prism(2.12, -1.2, -1, 0, 11.304);
    tie_wing_prism(2.12, -1.2, 1, 0, -11.304);
}


void draw_cylinder(double top_radius, double bottom_radius, double height, double color_r, double color_g, double color_b, bool hex){
    glBegin(GL_QUAD_STRIP);
    for (double angle = 0.0; angle <= 360; angle += 1){
        double x_top = Cos(angle) * top_radius;
        double y_top = Sin(angle) * top_radius;
        double x_bottom = Cos(angle) * bottom_radius;
        double y_bottom = Sin(angle) * bottom_radius;
        glColor3f(color_r, color_g, color_b);
        glVertex3f(x_top, y_top, height/2);
        glColor3f(color_r, color_g, color_b);
        glVertex3f(x_bottom, y_bottom, -height/2);
    }
    glEnd();

    // Create the top circle cap
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(color_r, color_g, color_b);
    glVertex3f(0, 0, height/2);
    for (double angle = 0.0; angle <= 360; angle += 1){
        double x_top = Cos(angle) * top_radius;
        double y_top = Sin(angle) * top_radius;
        glColor3f(color_r, color_g, color_b);
        glVertex3f(x_top, y_top, height/2);
    }
    glEnd();

    if (hex){
//        glEnable(GL_POLYGON_OFFSET_FILL);
//        glPolygonOffset(0,0);
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(0, 0, 0);
        glVertex3f(0, 0, -height/2 - 0.01);
        for (double angle = 0; angle <=360; angle += 60){
            double hex_x = Cos(angle) * top_radius / 2.5;
            double hex_y = Sin(angle) * top_radius / 2.5;
            glColor3f(color_r, color_g, color_b);
            glVertex3f(hex_x, hex_y, -height/2 -0.01);
        }
        glEnd();
//        glDisable(GL_POLYGON_OFFSET_FILL);

        // Left Engine
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(1, 0, 0);
        glVertex3f(-0.45, 0, -height/2);
        for (double angle = 0.0; angle <= 360; angle += 1){
            double x_bottom = Cos(angle) * .05 - .45;
            double y_bottom = Sin(angle) * .05;
            glColor3f(1, 0, 0);
            glVertex3f(x_bottom, y_bottom, -height/2);
        }
        glEnd();

        // Right Engine
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(1, 0, 0);
        glVertex3f(.45, 0, -height/2 - .01);
        for (double angle = 0.0; angle <= 360; angle += 1){
            double x_bottom = Cos(angle) * .05 + .45;
            double y_bottom = Sin(angle) * .05;
            glColor3f(1, 0, 0);
            glVertex3f(x_bottom, y_bottom, -height/2 - .01);
        }
        glEnd();

    }


    // Create the bottom circle cap
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(color_r, color_g, color_b);
    glVertex3f(0, 0, -height/2);
    for (double angle = 0.0; angle <= 360; angle += 1){
        double x_bottom = Cos(angle) * bottom_radius;
        double y_bottom = Sin(angle) * bottom_radius;
        glColor3f(color_r, color_g, color_b);
        glVertex3f(x_bottom, y_bottom, -height/2);
    }
    glEnd();
}


/*
 *  Modified code from spheres2
 */
static void cockpit_spheres(double x,double y,double z,double r,double color_r, double color_g, double color_b)
{
    const int d=5;
    int th,ph;

    //  Save transformation
    glPushMatrix();
    //  Offset and scale
    glTranslated(x,y,z);
    glScaled(r,r,r);
    //  Latitude bands
    glColor3f(color_r, color_g, color_b);
    for (ph=-90;ph<90;ph+=d)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=d)
        {
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph);
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph+d);
        }
        glEnd();
    }
    //  Undo transformations
    glPopMatrix();
}



void tie_fighter(double x, double y, double z, double scale){

    glPushMatrix();
    glTranslated(x, y, z);
    glScaled(scale, scale, scale);
    // Draw a grey sphere with a black circular window and a black truncated cone and hexagonal exhaust
    cockpit_spheres(0, 0, 0, 1, 0.6, 0.6, 0.6);
    cockpit_spheres(0.14, 0, 0, 0.9, 0, 0, 0); // Black window
    draw_cylinder(.5, .5, 5, 0.6, 0.6, 0.6);
    // Draw a frustum coming out of the cockpit
    glPushMatrix();
    glTranslated(-0.6, 0, 0);
    glRotated(90, 0, 1, 0);
    draw_cylinder( 1, .6, .9, 0.6, 0.6, 0.6, true);
    glPopMatrix();

    glPushMatrix();
    glTranslated(0, 0, 2.2);
    glScaled(1.5, 1.5, 1.5);

    tie_wing();
    glPopMatrix();

    glPushMatrix();
    glTranslated(0, 0, -2.2);
    glScaled(1.5, 1.5, 1.5);
    tie_wing();
    glPopMatrix();

    glPopMatrix();
}


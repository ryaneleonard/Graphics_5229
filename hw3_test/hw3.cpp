/*
 * Ryan Leonard
 * Homework 2
 *
 *
 * Citations:
 * For this homework assignment, I reused code from examples 8 and 25.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include "tie_fighter.h"
#include "CSCIx229.h"
//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle
int axes=1;       //  Display axes

int mode=1;       //  Projection mode
int fov=55;       //  Field of view
double asp=1;     //  Aspect ratio
double dim=5;   //  The Size of the World

// When not in fpv
double static_x = 0.0;
double static_y = 0.0;
double static_z = 0.0;

// The x, y, z position of the camera
double cam_x = 7.0;
double cam_y = 1.0;
double cam_z = 0.0;

int cam_th = 180;

// Lighting Values:
int light     =   1;  // Light Switch
int one       =   1;  // Unit value
int distance  =   5;  // Light distance
int inc       =  10;  // Ball increment
int smooth    =   1;  // Smooth/Flat shading
int local     =   0;  // Local Viewer Model
int emission  =   0;  // Emission intensity (%)
int ambient   =  30;  // Ambient intensity (%)
int diffuse   = 100;  // Diffuse intensity (%)
int specular  =   0;  // Specular intensity (%)
int shininess =   0;  // Shininess (power of two)
float shiny   =   1;  // Shininess (value)
int zh        =  90;  // Light azimuth
float ylight  =   0;  // Elevation of light

// Textures
unsigned int tie_texture;
unsigned int wing_texture;




//  Cosine and Sine in degrees
#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))


/*
 *  Draw vertex in polar coordinates
 */
static void Vertex(double th,double ph)
{
    double x = Sin(th)*Cos(ph);
    double y = Cos(th)*Cos(ph);
    double z =         Sin(ph);
    //  For a sphere at the origin, the position
    //  and normal vectors are the same
    glNormal3d(x,y,z);
    glTexCoord2d(th/360.0,ph/180.0+0.5);
    glVertex3d(x,y,z);
}


static void DrawAxes(double len){
    glBegin(GL_LINES);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(len,0.0,0.0);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(0.0,len,0.0);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(0.0,0.0,len);
    glEnd();
    //  Label axes
    glRasterPos3d(len,0.0,0.0);
    Print("X");
    glRasterPos3d(0.0,len,0.0);
    Print("Y");
    glRasterPos3d(0.0,0.0,len);
    Print("Z");
}

void cockpit_spheres2(double x,double y,double z,double r,double color_r, double color_g, double color_b)
{
    const int d=5;
    int th,ph;
    //  Save transformation
    glPushMatrix();
    //  Offset and scale
    glTranslated(x,y,z);
    glScaled(r,r,r);
    //  Latitude bands
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/tie_texture_2.bmp"));
    glColor3f(color_r, color_g, color_b);
    for (ph=-90;ph<90;ph+=d)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=d)
        {
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph);
            glColor3f(color_r, color_g, color_b);
            Vertex(th,ph+d);
        }
        glEnd();
    }
    //  Undo transformations
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}


void draw_laser(){
    glColor3f(0, 1, 0);
    const double PI = 3.14159;

    /* top triangle */
    double i, resolution  = 0.1;
    double height = 2;
    double top_radius = 0.02;
    double bottom_radius = 0.02;

    glPushMatrix();
//    glTranslatef(0, , 0);
// top cap
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, 1);
    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0,0,  height/2);  /* center */
    for (i = 4 * PI; i >= 0; i -= resolution)
    {
//        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(bottom_radius * cos(i),  bottom_radius * sin(i), height/2);
    }
    /* close the loop back to 0 degrees */
//    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(bottom_radius, 0, height/2);
    glEnd();

    // bottom cap
    /* bottom triangle: note: for is in reverse order */
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, -1);
//    glTexCoord2f( 0.5, 0.5 );
    glVertex3f(0, 0, -height/2);  /* center */
    for (i = 0; i <= 4 * PI; i += resolution)
    {
//        glTexCoord2f( 0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f );
        glVertex3f(top_radius * cos(i),  top_radius * sin(i), -height/2);
    }
    glEnd();

    /* middle tube */
    glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 4 * PI; i += resolution)
    {
        const float tc = ( i / (float)( 2 * PI ) );
//        glTexCoord2f( tc, 0.0 );
        glVertex3f(top_radius * cos(i), top_radius * sin(i), height/2);
        glNormal3f(top_radius * cos(i), top_radius * sin(i), 0);
//        glTexCoord2f( tc, 1.0 );
        glVertex3f(bottom_radius * cos(i), bottom_radius * sin(i), -height/2);
        glNormal3f(bottom_radius * cos(i), bottom_radius * sin(i), 0);

    }
    /* close the loop back to zero degrees */
    glTexCoord2f( 0.0, 0.0 );
    glVertex3f(top_radius,0, height/2);
    glTexCoord2f( 0.0, 1.0 );
    glVertex3f(bottom_radius,0,  -height/2);
    glEnd();

    glPopMatrix();
}

/*
 *  Use this to draw the light source
 */
static void ball(double x,double y,double z,double r)
{
    int th,ph;
    float yellow[] = {1.0,1.0,0.0,1.0};
    float Emission[]  = {0.0,0.0,0.01*emission,1.0};
    //  Save transformation
    glPushMatrix();
    //  Offset, scale and rotate
    glTranslated(x,y,z);
    glScaled(r,r,r);
    //  White ball
    glColor3f(1,1,1);
    glMaterialf(GL_FRONT,GL_SHININESS,shiny);
    glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
    glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
    //  Bands of latitude
    for (ph=-90;ph<90;ph+=inc)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=2*inc)
        {
            Vertex(th,ph);
            Vertex(th,ph+inc);
        }
        glEnd();
    }
    //  Undo transofrmations
    glPopMatrix();
}




/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
    const double len=1.5;  //  Length of axes
    //  Erase the window and the depth buffer
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //  Enable Z-buffering in OpenGL
    glEnable(GL_DEPTH_TEST);
    //  Undo previous transformations
    glLoadIdentity();
    // Orthogonal
    if (mode == 0)
    {
        glRotatef(ph,1,0,0);
        glRotatef(th,0,1,0);
    }
        // Perspective
    else if (mode == 1){
        static_x = -2*dim*Sin(th)*Cos(ph);
        static_y = +2*dim        *Sin(ph);
        static_z = +2*dim*Cos(th)*Cos(ph);
        gluLookAt(static_x,static_y,static_z , 0,0,0 , 0,Cos(ph),0);
    }
    else // (mode == 2)
    {
        gluLookAt(cam_x,cam_y,cam_z, cam_x + Cos(cam_th), 1, cam_z + Sin(cam_th), 0,1,0);
    }


    //  Flat or smooth shading
    glShadeModel(smooth ? GL_SMOOTH : GL_FLAT);

    //  Light switch
    if (light) {
        //  Translate intensity to color vectors
        float Ambient[] = {0.01 * ambient, 0.01 * ambient, 0.01 * ambient, 1.0};
        float Diffuse[] = {0.01 * diffuse, 0.01 * diffuse, 0.01 * diffuse, 1.0};
        float Specular[] = {0.01 * specular, 0.01 * specular, 0.01 * specular, 1.0};
        //  Light position
        float Position[] = {distance * Cos(zh), ylight, distance * Sin(zh), 1.0};
        //  Draw light position as ball (still no lighting here)
        glColor3f(1, 1, 1);
        ball(Position[0], Position[1], Position[2], 0.1);
        //  OpenGL should normalize normal vectors
        glEnable(GL_NORMALIZE);
        //  Enable lighting
        glEnable(GL_LIGHTING);
        //  Location of viewer for specular calculations
        glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, local);
        //  glColor sets ambient and diffuse color materials
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
        glEnable(GL_COLOR_MATERIAL);
        //  Enable light 0
        glEnable(GL_LIGHT0);
        //  Set ambient, diffuse, specular components and position of light 0
        glLightfv(GL_LIGHT0, GL_AMBIENT, Ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, Diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, Specular);
        glLightfv(GL_LIGHT0, GL_POSITION, Position);
    }


    draw_laser();

//    tie_fighter(3, 0, 0, 0.2, tie_texture, wing_texture);
//    glPushMatrix();
//    glRotated(30, 1, 1, 0);
//    glRotated(-45, 0, 0, 1);
//    glScaled(.75, .75, .75);
//    tie_fighter(-2, 2, 0, 0.2, tie_texture, wing_texture);
//    glPopMatrix();
    // Lasers
//    glPushMatrix();
//    glTranslated(1.7, 0.3, -0.4);
//    glRotated(-50, 0, 0, 1);
//    glRotated(120, 0, 1, 0);
//
//    glPushMatrix();
//    glTranslated(0.1, 0, 1);
//    draw_cylinder(0.01, 0.01, 1, 0, 1, 0, false, 0);
//    glPopMatrix();
//    draw_cylinder(0.01, 0.01, 1, 0, 1, 0, false, 0);
//    glPopMatrix();

    //  Draw axes in white
    glColor3f(1, 1, 1);
    if (axes) {
        DrawAxes(len);
    }

    //  Display parameters
    glWindowPos2i(5,5);
    Print("Angle=%d,%d  dim=%.1f FOV=%d Projection=%d  cam_th=%d",
          th,ph,dim,fov,mode, cam_th);

    glWindowPos2i(35,35);
    Print("cam_x=%f cam_z=%f cam_th=%d",
          cam_x, cam_z, cam_th);

    //  Render the scene
    glFlush();
    //  Make the rendered scene visible
    glutSwapBuffers();
}



/* A working display function to refer back to for when I screw this up */
/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
//void display()
//{
//    const double len=1.5;  //  Length of axes
//    //  Erase the window and the depth buffer
//    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
//    //  Enable Z-buffering in OpenGL
//    glEnable(GL_DEPTH_TEST);
//    //  Undo previous transformations
//    glLoadIdentity();
//    // Orthogonal
//    if (mode == 0)
//    {
//        glRotatef(ph,1,0,0);
//        glRotatef(th,0,1,0);
//    }
//        // Perspective
//    else if (mode == 1){
//        static_x = -2*dim*Sin(th)*Cos(ph);
//        static_y = +2*dim        *Sin(ph);
//        static_z = +2*dim*Cos(th)*Cos(ph);
//        gluLookAt(static_x,static_y,static_z , 0,0,0 , 0,Cos(ph),0);
//    }
//    else // (mode == 2)
//    {
//        gluLookAt(cam_x,cam_y,cam_z, cam_x + Cos(cam_th), 1, cam_z + Sin(cam_th), 0,1,0);
//    }
//
//    tie_fighter(3, 0, 0, 0.2);
//    glPushMatrix();
//    glRotated(30, 1, 1, 0);
//    glRotated(-45, 0, 0, 1);
//    glScaled(.75, .75, .75);
//    tie_fighter(-2, 2, 0, 0.2);
//    glPopMatrix();
//    // Lasers
//    glPushMatrix();
//    glTranslated(1.7, 0.3, -0.4);
//    glRotated(-50, 0, 0, 1);
//    glRotated(120, 0, 1, 0);
//
//    glPushMatrix();
//    glTranslated(0.1, 0, 1);
//    draw_cylinder(0.01, 0.01, 1, 0, 1, 0);
//    glPopMatrix();
//    draw_cylinder(0.01, 0.01, 1, 0, 1, 0);
//    glPopMatrix();
//
//    //  Draw axes in white
//    glColor3f(1, 1, 1);
//    if (axes) {
//        DrawAxes(len);
//    }
//
//    //  Display parameters
//    glWindowPos2i(5,5);
//    Print("Angle=%d,%d  dim=%.1f FOV=%d Projection=%d  cam_th=%d",
//          th,ph,dim,fov,mode, cam_th);
//
//    glWindowPos2i(35,35);
//    Print("cam_x=%f cam_z=%f cam_th=%d",
//          cam_x, cam_z, cam_th);
//
//    //  Render the scene
//    glFlush();
//    //  Make the rendered scene visible
//    glutSwapBuffers();
//}







/*
 *  GLUT calls this routine when an arrow key is pressed (Modified from ex25)
 */
void special(int key,int x,int y)
{
    // use the arrow keys to change the viewing angle
    if (key == GLUT_KEY_UP)
        ph += 5;
    else if (key == GLUT_KEY_DOWN)
        ph -= 5;
    else if (key == GLUT_KEY_LEFT)
        th -= 5;
    else if (key == GLUT_KEY_RIGHT)
        th += 5;
    else if (key == GLUT_KEY_PAGE_DOWN)
        dim += 0.1;
    else if (key == GLUT_KEY_PAGE_UP && dim>1)
        dim -= 0.1;
    //  Keep angles to +/-360 degrees
    th %= 360;
    ph %= 360;
    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
    // First Person Translation
    if (mode == 2) {
        // Move the camera center and the world center in what is currently defined as forward.
        if ((ch == 'w' || ch == 'W')) {
            cam_x += Cos(cam_th);
            cam_z += Sin(cam_th);
        }
            // Move the world center left by 5 degrees
        else if (ch == 'a' || ch == 'A'){
            cam_th -= 5;
            cam_th %=360;
        }
        else if (ch == 'd' || ch == 'D'){
            cam_th += 5;
            cam_th %=360;
        }
        else if (ch == 's' || ch == 'S'){
            cam_x -= Cos(cam_th);
            cam_z -= Sin(cam_th);
        }
    }

    //  Exit on ESC
    if (ch == 27)
        exit(0);
        //  Reset all parameters
    else if (ch == 'r' || ch == 'R') {
        th = 0;
        ph = 0;
        cam_x = -1.13;
        cam_y = 1;
        cam_z = 0.13;

        static_x = 0.0;
        static_y = 0.0;
        static_z = 0.0;

        cam_th = 0;
        dim = mode == 2?10:5;

    }
        //  Toggle axes
    else if (ch == 't' || ch == 'T')
        axes = 1-axes;
        // Orthogonal
    else if (ch == '1'){
        mode = 0;
        dim = 5;
    }
        //Perspective
    else if (ch == '2'){
        mode = 1;
        dim = 5;
    }
        //first person
    else if (ch == '3') {
        mode = 2;
        dim = 10;
    }

    //  Reproject
    Project(mode?fov:0,asp,5);

    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}

// Taken from ex25 because ex8 didnt work
void reshape(int width,int height)
{
    //  Ratio of the width to the height of the window
    asp = (height>0) ? (double)width/height : 1;
    //  Set the viewport to the entire window
    glViewport(0,0, width,height);
    //  Set projection
    Project(fov,asp,dim);
}


/*
 *  GLUT calls this routine when the window is resized
 */
void idle()
{
    //  Elapsed time in seconds
    double t = glutGet(GLUT_ELAPSED_TIME)/1000.0;
    zh = fmod(90*t,360.0);
    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}


/*
 *  Start up GLUT and tell it what to do
 */
// Taken from examples
int main(int argc,char* argv[])
{
    //  Initialize GLUT and process user parameters
    glutInit(&argc,argv);
    //  Request double buffered, true color window with Z buffering at 600x600
    glutInitWindowSize(1000,1000);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    //  Create the window
    glutCreateWindow("Ryan Leonard Homework 2");
    //  Tell GLUT to call "display" when the scene should be drawn
    glutDisplayFunc(display);
    //  Tell GLUT to call "reshape" when the window is resized
    glutReshapeFunc(reshape);
    //  Tell GLUT to call "special" when an arrow key is pressed
    glutSpecialFunc(special);
    //  Tell GLUT to call "key" when a key is pressed
    glutKeyboardFunc(key);
    glutIdleFunc(idle);
    tie_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/tie_texture_2.bmp");
    wing_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/pinstripe_small.bmp");
//    tie_texture = LoadTexBMP("tie_texture_2.bmp");
//    wing_texture = LoadTexBMP("pinstripe_small.bmp");
    //  Pass control to GLUT so it can interact with the user
    glutMainLoop();
    return 0;
}
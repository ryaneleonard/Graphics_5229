//  Set the fragment color

uniform sampler2D tex;

void main()
{
    if (gl_Color[0] > 0.6 && gl_Color[1] > 0.6 && gl_Color[2] > 0.6)
        discard;
    else{
        gl_FragColor = gl_Color * texture2D(tex,gl_TexCoord[0].xy);
        }
}

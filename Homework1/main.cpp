// Ryan Leonard
// This program uses the code from example 6 and the provided lorenz.c file to create a 3D view of the lorenz generator
// with variable parameters and viewing angles.

// As far as impressing my classmates, I spent waaay too much time failing to get GLFW working on my system to do anything
// beyond the bare minimum

// Getting glut working on my system took around 5 minutes, and this assignment took around 2-3 hours to finish.
// However, I spent all of Saturday and a good portion of sunday trying to get all of the glfw examples I could find to work on my machine.


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <iostream>

//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//  Globals
int th=0;       // Azimuth of view angle
int ph=0;       // Elevation of view angle
double w=0.02;     // W variable
double dim=2;   // Dimension of orthogonal box


// Default Lorenz Parameters:
double s = 10;
double b = 2.5;
double r = 28;

double points[50000][3]; // Create an array to store all of the points that will be generated.



// From lorenz.c
void lorenz_generator(){

    double x = 1;
    double y = 1;
    double z = 1;
    int i = 0;
    /*  Time step  */
    double dt = 0.001;
    /*
    *  Integrate 50,000 steps (50 time units with dt = 0.001)
    *  Explicit Euler integration
    */
    for (i = 0; i<50000; i++)
    {
        double dx = s*(y-x);
        double dy = x*(r-z)-y;
        double dz = x*y - b*z;
        x += dt*dx;
        y += dt*dy;
        z += dt*dz;
        points[i][0] = x*w;
        points[i][1] = y*w;
        points[i][2] = z*w;
    }

}


/*
 *  Convenience routine to output raster text
 *  Use VARARGS to make this more flexible
 */
#define LEN 8192  // Maximum length of text string
void Print(const char* format , ...)
{
    char    buf[LEN];
    char*   ch=buf;
    va_list args;
    //  Turn the parameters into a character string
    va_start(args,format);
    vsnprintf(buf,LEN,format,args);
    va_end(args);
    //  Display the characters one at a time at the current raster position
    while (*ch)
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*ch++); // Playing with other available font options
}



void display_lorenz(){
    // Create Data Points
    lorenz_generator();

    // Clear the image
    glClear(GL_COLOR_BUFFER_BIT);

    //  Reset previous transforms
    glLoadIdentity();

    // Set the View Angle
    glRotated(ph, 1, 0, 0);
    glRotated(th, 0, 1, 0);

    // Use a line strip to sequentially connect the points in the lorenz attractor
    glBegin(GL_LINE_STRIP);
    for(int i = 0; i < 50000; i++){
        glColor3d(points[i][0] * points[i][1] * 3.0 + 0.1, points[i][1] *points[i][2] * 3 + 0.1, points[i][2] * points[i][0] * 3 + 0.1); // Playing with variable color values
        glVertex3dv(points[i]);
    }
    glEnd();  // End the GL Line Strip
    //  Draw axes in white
    glColor3f(1,1,1);
    glBegin(GL_LINES);
    glVertex3d(0,0,0);
    glVertex3d(1,0,0);
    glVertex3d(0,0,0);
    glVertex3d(0,1,0);
    glVertex3d(0,0,0);
    glVertex3d(0,0,1);
    glEnd();
    //  Label axes
    glRasterPos3d(1,0,0);
    Print("X");
    glRasterPos3d(0,1,0);
    Print("Y");
    glRasterPos3d(0,0,1);
    Print("Z");
    //  Display parameters
    glWindowPos2i(5,5);
    Print("View Angle=%d,%d  ",th,ph);
    Print("View Angle=%d,%d; Lorenz Parameters: sigma=%f, rho=%f, beta=%f", th, ph, s, b, r);
    //  Flush and swap
    glWindowPos2i(5, 50);
    Print("The keys q, w, e, a, s and d alter the lorenz parameters");

    glFlush();
    glutSwapBuffers();
}


/*
 *  GLUT calls this routine when an arrow key is pressed
 */
void special(int key,int x,int y)
{
    //  Right arrow key - increase azimuth by 5 degrees
    if (key == GLUT_KEY_RIGHT)
        th += 5;
        //  Left arrow key - decrease azimuth by 5 degrees
    else if (key == GLUT_KEY_LEFT)
        th -= 5;
        //  Up arrow key - increase elevation by 5 degrees
    else if (key == GLUT_KEY_UP)
        ph += 5;
        //  Down arrow key - decrease elevation by 5 degrees
    else if (key == GLUT_KEY_DOWN)
        ph -= 5;
    //  Keep angles to +/-360 degrees
    th %= 360;
    ph %= 360;
    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}


void lorenz_params(unsigned char k, int x, int y){
    // Depending on the key input values increment or decrement the s b and r values of the lorenz attractor
    if (k == 'q')
        s +=0.5;
    else if (k == 'a')
        s -=0.5;
    else if (k == 'w')
        b += 0.5;
    else if (k == 's')
        b -= 0.5;
    else if (k == 'e')
        r += 0.5;
    else if (k == 'd')
        r -= 0.5;
    /* Question: Why is it necessary to define this keypress in both of the key functions?
     * Specifically, I had both this function and the key function originally present in example 6, but I needed
     * add a case for the escape key here, or else I wasnt able to close the window...
     */

    //  Exit on ESC
    else if (k == 27)
        exit(0);

    // Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}

/*
 *  GLUT calls this routine when the window is resized
 */
void reshape(int width,int height)
{
    //  Ratio of the width to the height of the window
    double w2h = (height>0) ? (double)width/height : 1;
    //  Set the viewport to the entire window
    glViewport(0,0, width,height);
    //  Tell OpenGL we want to manipulate the projection matrix
    glMatrixMode(GL_PROJECTION);
    //  Undo previous transformations
    glLoadIdentity();
    //  Orthogonal projection box adjusted for the
    //  aspect ratio of the window
    glOrtho(-dim*w2h,+dim*w2h, -dim,+dim, -dim,+dim);
    //  Switch to manipulating the model matrix
    glMatrixMode(GL_MODELVIEW);
    //  Undo previous transformations
    glLoadIdentity();
}

/*
 *  Start up GLUT and tell it what to do
 */
int main(int argc,char* argv[])
{
    //  Initialize GLUT and process user parameters
    glutInit(&argc,argv);
    //  Request double buffered, true color window
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    //  Request 500 x 500 pixel window
    glutInitWindowSize(1000,1000);
    //  Create the window
    glutCreateWindow("Ryan Leonard's Lorenz Attractor");
    //  Tell GLUT to call "display_lorenz" when the scene should be drawn
    glutDisplayFunc(display_lorenz);
    //  Tell GLUT to call "reshape" when the window is resized
    glutReshapeFunc(reshape);
    //  Tell GLUT to call "special" when an arrow key is pressed
    glutSpecialFunc(special);
    // Tell GLUT to call lorenz_params when key is pressed
    glutKeyboardFunc(lorenz_params);
    //  Pass control to GLUT so it can interact with the user
    glutMainLoop();
    //  Return code
    return 0;
}

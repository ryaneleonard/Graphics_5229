## build instructions:
cmake .
make
./project

##Controls:

w: accelerate
s: decellerate
a: spin left
d: spin right

Arrow Keys: Steering

Space bar: Fire lasers


What to look for:

1. Laser Lighting
Your ship can fire as many lasers as you like. Each laser can only exist for a finite time, and will automatically remove itself after its time has expired.
Each Laser acts as an additional green light source.

2. Collision Detection
Each object in the scene, with the exception of the primary light source has an axis aligned bounding box.
Some objects are impervious to others:
Lasers can not be destroyed by other objects
Other ships can be destroyed by lasers, or by a collision with your ship.
The death star can be destroyed with lasers, or it can be destroyed by a collision with your ship
Your ship can only be destroyed by driving into the death star

3. Billboarding
When you destroy an arbitrary object, it creates an explosion effect. You are able to navigate to any location in the scene, and the effect will always face the camera.

4. Model loading
NOTE: I intentionally broke the materials file. When the materials file was properly loaded, all of the correctly mapped textures disappeared, and I was left with a white model. When the program cant find the materials file, everything works.
A collection of models have been loaded into the scene. They have some imperfections when mapping textures, and for the most part, problems with correcting normals have been manually corrected.

5. Skybox
The Skybox is sensitive to specific events. If you are brave enough to slam on the gas, (if speed is greater than 2.0), you will enter maximum plaid mode, in which changes the texture of the skybox to a plaid texture.
Additionally, when you destroy your ship in any way, you are then invited to join the empire with a recruitment poster.

6. Shaders
The textures loaded for the collision effects were modified using a shader to remove the background pixels and only display the object of interest

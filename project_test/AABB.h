//
// Created by ryan on 7/2/18.
//

#ifndef PATENT_INFRINGEMENT_TIMEBOMB_AABB_H
#define PATENT_INFRINGEMENT_TIMEBOMB_AABB_H

#include <GL/glut.h>
#include <iostream>


class AABB {
public:
    AABB(){}
    ~AABB(){}

    void initialize(double center_x, double center_y, double center_z, double rad_x, double rad_y, double rad_z){
            bbox_center[0] = center_x; bbox_center[1] = center_y; bbox_center[2] = center_z;
            bbox_radius[0] = rad_x; bbox_radius[1] = rad_y; bbox_radius[2] = rad_z;
    }
    bool isCollided(AABB &some_object);

    // The center coordinate of the object being bounded
    double bbox_center[3];
    // The Max X, Y & Z coordinate of the figure
    double bbox_radius[3];
    // The Min X, Y & Z coordinate will be equal to: center[i] - (max[i] - center[i])

    void update_center(double x, double y, double z){
        bbox_center[0] = x;
        bbox_center[1] = y;
        bbox_center[2] = z;
    }

    double min_x();
    double max_x();
    double min_y();
    double max_y();
    double min_z();
    double max_z();


private:
    bool isInside(double x, double y, double z, AABB &some_object);
};



#endif //PATENT_INFRINGEMENT_TIMEBOMB_AABB_H

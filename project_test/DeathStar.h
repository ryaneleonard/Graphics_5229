//
// Created by ryan on 7/2/18.
//

#ifndef COPYRIGHT_INFRINGEMENT_TIMEBOMB_DEATHSTAR_H
#define COPYRIGHT_INFRINGEMENT_TIMEBOMB_DEATHSTAR_H
#include <GL/glut.h>
#include <math.h>
#include "AABB.h"

#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))

class DeathStar {
public:
    DeathStar(double x, double y, double z, double scale, unsigned int texture) :
    pos_x_(x), pos_y_(y), pos_z_(z), scale_(scale), texture_(texture)
    {
        this->bbox.initialize(x, y, z, scale, scale, scale);
    }
    ~DeathStar(){}

    void draw_deathstar();

    bool should_exist_ = true;

    AABB bbox;
    double get_pos_x(){
        return this->pos_x_;
    }

    double get_pos_y(){
        return this->pos_y_;
    }

    double get_pos_z(){
        return this->pos_z_;
    }

private:
    void Vertex(double th,double ph);
    double pos_x_, pos_y_, pos_z_, scale_;
    unsigned int texture_;
};



#endif //COPYRIGHT_INFRINGEMENT_TIMEBOMB_DEATHSTAR_H

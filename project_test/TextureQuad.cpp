//
// Created by ryan on 7/2/18.
//

#include "TextureQuad.h"


void TextureQuad::draw_quad() {

    // Lighting
    float white[] = {1,1,1,1};
    float Emission[]  = {0.0,0.0,0.01,1.0};
    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,1);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,white);
    glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,Emission);



    glColor3f(1,1,1);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV , GL_TEXTURE_ENV_MODE ,GL_MODULATE);
    glBindTexture(GL_TEXTURE_2D, this->texture_);
    glPushMatrix(); // Translate the explosion to appropriate location
    glPushMatrix(); // Continuously rotate the quad to face the camera
    glTranslated(this->x_, this->y_, this->z_);

    glRotated(-this->ph_, 0, 1, 0);
    glRotated(-this->th_, 1, 0, 0);

    glPushMatrix(); // Continuously rotate the storm trooper/explosion in place
    glRotated(rotation, 0, 0, 1);
    glScaled(this->size_, this->size_, this->size_);
    glBegin(GL_QUADS);
    glNormal3d(0, 0, -1);
    glTexCoord2f(0.0,0.0); glVertex3f(-1,-1,0);
    glTexCoord2f(1,0.0); glVertex3f(+1,-1,0);
    glTexCoord2f(1,1); glVertex3f( 1, 1,0);
    glTexCoord2f(0.0,1); glVertex3f(-1,+1,0);
    glEnd();
    glPopMatrix(); // Continuously rotate the storm trooper/explosion in place
    glPopMatrix(); // Continuously rotate the quad to face the camera
    glPopMatrix(); // Translate the explosion to appropriate location

    glDisable(GL_TEXTURE_2D);
}


void TextureQuad::update_quad_timer(double time){
    quad_timer += time;
    this->rotation += 1;
}


bool TextureQuad::quad_should_exist() {
    // if the quad has existed for more than 3 seconds, get rid of it.
    return quad_timer > 8000;
}

void TextureQuad::update_angles(double th, double ph){
    // TODO: This is a hack... Fix the source
    this->ph_ = th;
    this->th_ = ph;
}

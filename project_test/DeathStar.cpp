//
// Created by ryan on 7/2/18.
//

#include "DeathStar.h"

void DeathStar::Vertex(double th,double ph)
{
    double x = Sin(th)*Cos(ph);
    double y = Cos(th)*Cos(ph);
    double z =         Sin(ph);
    //  For a sphere at the origin, the position
    //  and normal vectors are the same
    glNormal3d(x,y,z);
    glTexCoord2d(th/360.0,ph/180.0+0.5);
    glVertex3d(x,y,z);

}


void DeathStar::draw_deathstar(){
    const int d=5;
    int th,ph;
    //  Save transformation
    glPushMatrix();
    //  Offset and scale
    glTranslated(this->pos_x_,this->pos_y_,this->pos_z_);
    glRotated(-90, 1, 0, 0);

    glScaled(this->scale_, this->scale_, this->scale_);
    //  Latitude bands
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this->texture_);
    glColor3f(1, 1, 1);
    for (ph=-90;ph<90;ph+=d)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=d)
        {
            glColor3f(1, 1, 1);
            Vertex(th,ph);
            glColor3f(1, 1, 1);
            Vertex(th,ph+d);
        }
        glEnd();
    }
    //  Undo transformations
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}



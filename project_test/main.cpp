// Much of this code was modified from the examples in class, however, I believe that that was the only external source used.
// The primary examples from which code was taken were examples 25 and 27
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include "CSCIx229.h"
#include "TieClass.h"
#include "laser.h"
#include "DeathStar.h"
#include "TextureQuad.h"
#include "ObjectModel.h"
#include <list>
#include <iostream>

// TODO: turn off ambient light for lasers
// TODO: Turn down diffuse light for starlight

//  OpenGL with prototypes for glext
#define GL_GLEXT_PROTOTYPES
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <glm/vec3.hpp>
#endif

int th=0;         //  Azimuth of view angle
int ph=0;         //  Elevation of view angle

int rho=0;
int axes=0;       //  Display axes

int mode=1;       //  Projection mode
int fov=55;       //  Field of view
double asp=1;     //  Aspect ratio
double dim=20;   //  The Size of the World

int    fly=1;    //  Animated flight


// 3rd person camera
double X  = 0;   //  Location
double Y  = 0;   //  Location
double Z  = 0;   //  Location
double Dx = 1;   //  Direction
double Dy = 0;   //  Direction
double Dz = 0;   //  Direction
double Ux = 0;   //  Up
double Uy = 1;   //  Up
double Uz = 0;   //  Up
double Ox = 0;   //  LookAt
double Oy = 0;   //  LookAt
double Oz = 0;   //  LookAt
double Ex = 0;   //  Eye
double Ey = 0;   //  Eye
double Ez = -1;   //  Eye

// Create a global vector to store the state of all lasers
std::list<Laser> lasers;
std::list<TextureQuad> storm_troopers;
std::list<TextureQuad> explosions;

bool death_star_should_exist = true;
bool tie1_should_exist = true;
bool tie2_should_exist = true;
bool tie3_should_exist = true;
bool bwing_should_exist = true;
bool awing_should_exist = true;
bool pilot_tie_should_exist = true;



// Previous Direction Vector:
double prev_Dx, prev_Dy, prev_Dz = 0;
double prev_Ux, prev_Uy, prev_Uz = 0;
double speed = 0.2;

// Fighter Position
double roll=0;   //  Roll angle
double pitch=0;  //  Pitch angle
double yaw=0;    //  Yaw angle




// My Previous implementation of 3rd person motion.
// When not in fpv
//double static_x = 1.0;
//double static_y = 0.0;
//double static_z = 0.0;

//// The x, y, z position of the camera
//double cam_x = 0.0;
//double cam_y = 1.0;
//double cam_z = 0.0;
//
//// Position of the tie fighter
//double pilot_x = 4;
//double pilot_y = 0;
//double pilot_z = 0;


int cam_th = 0;

int    box=1;    //  Draw sky
int    sky[2];   //  Sky textures
unsigned int    spaceland;


// Lighting Values:
int light     =   1;  // Light Switch
int one       =   1;  // Unit value
int distance1  =   10;  // tie1 distance
int distance2  =   50;  // tie2 distance
int distance3  =   30;  // tie3 distance
int inc       =  10;  // Ball increment
int smooth    =   1;  // Smooth/Flat shading
int local     =   0;  // Local Viewer Model
int emission  =   0;  // Emission intensity (%)
int ambient   =  30;  // Ambient intensity (%)
int diffuse   = 100;  // Diffuse intensity (%)
int specular  =   0;  // Specular intensity (%)
int shininess =   0;  // Shininess (power of two)
float shiny   =   1;  // Shininess (value)
int zh        =  90;  // Light azimuth
float ylight  =   0;  // Elevation of light
int pause = 0;

// Textures
unsigned int tie_texture;
unsigned int wing_texture;
unsigned int death_star_texture;
unsigned int explosion_texture;
unsigned int storm_trooper_texture;
unsigned int plaid;
unsigned int recruitment;

// A-Wing stuff
int a_tex[2];
int awing=0;

int b_tex[2];
int bwing=0;

int stormtrooper_shader, explosion_shader;

//  Cosine and Sine in degrees
#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))



/*
 *  Draw vertex in polar coordinates
 */
void Vertex(double th,double ph)
{
    double x = Sin(th)*Cos(ph);
    double y = Cos(th)*Cos(ph);
    double z =         Sin(ph);
    //  For a sphere at the origin, the position
    //  and normal vectors are the same
    glNormal3d(x,y,z);
    glTexCoord2d(th/360.0,ph/180.0+0.5);
    glVertex3d(x,y,z);
}


void DrawAxes(double len){
    glBegin(GL_LINES);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(len,0.0,0.0);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(0.0,len,0.0);
    glVertex3d(0.0,0.0,0.0);
    glVertex3d(0.0,0.0,len);
    glEnd();
    //  Label axes
    glRasterPos3d(len,0.0,0.0);
    Print("X");
    glRasterPos3d(0.0,len,0.0);
    Print("Y");
    glRasterPos3d(0.0,0.0,len);
    Print("Z");
}

void Sky(double D)
{
    glColor3f(1,1,1);
    glEnable(GL_TEXTURE_2D);

    if (pilot_tie_should_exist && speed < 2) {
        //  Sides
        glBindTexture(GL_TEXTURE_2D, spaceland);
        glBegin(GL_QUADS);
        // middle bit
        glTexCoord2f(0.00, 0.34);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(0.25, 0.34);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(0.25, .66);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(0.00, .66);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0.25, 0.34);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(0.50, 0.34);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(0.50, 0.66);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(0.25, 0.66);
        glVertex3f(+D, +D, -D);

        glTexCoord2f(0.50, 0.34);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(0.75, 0.34);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(0.75, 0.66);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(0.50, 0.66);
        glVertex3f(+D, +D, +D);

        glTexCoord2f(0.75, 0.34);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(1.00, 0.34);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(1.00, 0.66);
        glVertex3f(-D, +D, -D);
        glTexCoord2f(0.75, 0.66);
        glVertex3f(-D, +D, +D);
        glEnd();

        //  Top and bottom
        glBindTexture(GL_TEXTURE_2D, spaceland);
        glBegin(GL_QUADS);
        glTexCoord2f(0.25, 0.66);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(0.5, 0.66);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(0.5, 1);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(0.25, 1);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0.25, 0);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(0.49, 0);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(0.49, 0.33);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(0.25, 0.33);
        glVertex3f(-D, -D, -D);
        glEnd();

    }
    else if (pilot_tie_should_exist && speed > 2){
        //  Sides
        glBindTexture(GL_TEXTURE_2D, plaid);
        glBegin(GL_QUADS);
        // middle bit
        glTexCoord2f(0., 0);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0., 0);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(+D, +D, -D);

        glTexCoord2f(0., 0);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(0, 1);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(+D, +D, +D);

        glTexCoord2f(0., 0);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(0, 1);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, +D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, +D);
        glEnd();

        //  Top and bottom
        glBindTexture(GL_TEXTURE_2D, plaid);
        glBegin(GL_QUADS);
        glTexCoord2f(0., 0);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0., 0);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, -D, -D);
        glEnd();
    }
    else{
        //  Sides
        glBindTexture(GL_TEXTURE_2D, recruitment);
        glBegin(GL_QUADS);
        // middle bit
        D = -D;
        glTexCoord2f(0., 1);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(0, 0);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0., 1);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(0, 0);
        glVertex3f(+D, +D, -D);

        glTexCoord2f(0., 1);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(0, 0);
        glVertex3f(+D, +D, +D);

        glTexCoord2f(0., 1);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, -D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, -D);
        glTexCoord2f(0, 0);
        glVertex3f(-D, +D, +D);
        glEnd();

        //  Top and bottom
        glBindTexture(GL_TEXTURE_2D, recruitment);
        glBegin(GL_QUADS);
        glTexCoord2f(0., 0);
        glVertex3f(+D, +D, -D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, +D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(-D, +D, +D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, +D, -D);

        glTexCoord2f(0., 0);
        glVertex3f(-D, -D, +D);
        glTexCoord2f(0, 1);
        glVertex3f(+D, -D, +D);
        glTexCoord2f(1, 1);
        glVertex3f(+D, -D, -D);
        glTexCoord2f(1, 0);
        glVertex3f(-D, -D, -D);
        glEnd();

    }
    glDisable(GL_TEXTURE_2D);
}

// Draw F16's flying in formation
static void DrawFlight(double x0,double y0,double z0,
                       double dx,double dy,double dz,
                       double ux,double uy,double uz)
{
/*    int i,k;
    //  Position of members
    double X[] = {-1.0,+1.0,+0.0,+0.0};
    double Y[] = {-0.3,+0.3,+0.0,+0.0};
    double Z[] = {+0.0,+0.0,-1.5,+1.5};
    //  Unit vector in direction of flght
    double D0 = sqrt(dx*dx+dy*dy+dz*dz);
    double X0 = dx/D0;
    double Y0 = dy/D0;
    double Z0 = dz/D0;
    //  Unit vector in "up" direction
    double D1 = sqrt(ux*ux+uy*uy+uz*uz);
    double X1 = ux/D1;
    double Y1 = uy/D1;
    double Z1 = uz/D1;
    //  Cross product gives the third vector
    double X2 = Y0*Z1-Y1*Z0;
    double Y2 = Z0*X1-Z1*X0;
    double Z2 = X0*Y1-X1*Y0;
    //  Rotation matrix
    double M[16] = {X0,Y0,Z0,0 , X1,Y1,Z1,0 , X2,Y2,Z2,0 , 0,0,0,1};

    //  Save current transforms
    glPushMatrix();
    //  Offset and rotate
    glTranslated(x0,y0,z0);
    glMultMatrixd(M);

    glPushMatrix();
    glTranslated(X[0],Y[0],Z[0]);
    glRotated(yaw,0,1,0);
//    glRotated(pitch,0,0,1);
//    glRotated(roll,1,0,0);
    tie_fighter(0, 0, 0, 0.1, tie_texture, wing_texture);
//            glCallList(F16[k]);
    glPopMatrix();

    //  Undo transformations
    glPopMatrix();*/



}


/*
 *  Use this to draw the light source
 */
static void ball(double x,double y,double z,double r)
{
    int th,ph;
    float yellow[] = {1.0,1.0,0.0,1.0};
    float Emission[]  = {0.0,0.0,0.01*emission,1.0};
    //  Save transformation
    glPushMatrix();
    //  Offset, scale and rotate
    glTranslated(x,y,z);
    glScaled(r,r,r);
    //  White ball
    glColor3f(1,1,1);
    glMaterialf(GL_FRONT,GL_SHININESS,shiny);
    glMaterialfv(GL_FRONT,GL_SPECULAR,yellow);
    glMaterialfv(GL_FRONT,GL_EMISSION,Emission);
    //  Bands of latitude
    for (ph=-90;ph<90;ph+=inc)
    {
        glBegin(GL_QUAD_STRIP);
        for (th=0;th<=360;th+=2*inc)
        {
            Vertex(th,ph);
            Vertex(th,ph+inc);
        }
        glEnd();
    }
    //  Undo transofrmations
    glPopMatrix();
}




/*
 *  OpenGL (GLUT) calls this routine to display the scene
 */
void display()
{
    const double len=1.5;  //  Length of axes
    //  Erase the window and the depth buffer
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //  Enable Z-buffering in OpenGL
    glEnable(GL_DEPTH_TEST);
    //  Undo previous transformations
    glLoadIdentity();




    gluLookAt(Ox - 5 * Dx, Oy - 5 * Dy, Oz - 5 * Dz, Ox, Oy, Oz, 0, 1, 0);
    Sky(3.5 * dim);

    //  Flat or smooth shading
    glShadeModel(smooth ? GL_SMOOTH : GL_FLAT);
    //  Light switch
    if (light) {
        //  Translate intensity to color vectors
        float Ambient[] = {0.01 * ambient, 0.01 * ambient, 0.01 * ambient, 1.0};
        float Diffuse[] = {0.01 * diffuse, 0.01 * diffuse, 0.01 * diffuse, 1.0};
        float Specular[] = {0.01 * specular, 1 * specular, 0.01 * specular, 1.0};
        //  Light position
//            float Position[] = {distance * Cos(zh), ylight, distance * Sin(zh), 1.0};
        float Position[] = {-30, 30, -30, 1.0};
        //  Draw light position as ball (still no lighting here)
        glColor3f(0, 1, 0);
        ball(Position[0], Position[1], Position[2], 0.1);
        //  OpenGL should normalize normal vectors
        glEnable(GL_NORMALIZE);
        //  Enable lighting
        glEnable(GL_LIGHTING);
        //  Location of viewer for specular calculations
        glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, local);
        //  glColor sets ambient and diffuse color materials
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
        glEnable(GL_COLOR_MATERIAL);
        //  Enable light 0
        glEnable(GL_LIGHT0);
        //  Set ambient, diffuse, specular components and position of light 0
        glLightfv(GL_LIGHT0, GL_AMBIENT, Ambient);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, Diffuse);
        glLightfv(GL_LIGHT0, GL_SPECULAR, Specular);
        glLightfv(GL_LIGHT0, GL_POSITION, Position);
    }


    Sky(3.5 * dim);
    // Tie Formation Position:
    float Position1[] = {distance1 * Cos(zh), 0, distance1 * Sin(zh), 1.0};
    float Position2[] = {-distance2 * Cos(zh), 0, distance2 * Sin(zh), 1.0};
    float Position3[] = { 0,distance3 * Cos(zh), distance3 * Sin(zh), 1.0};


    // Draw lasers prior to the creation of other objects for cool effects
    for (auto &laser: lasers){
        laser.draw_laser();
    }
    TieClass tieOne(Position1[0], Position1[1], Position1[2], 0, 0, zh, 0.2, tie_texture, wing_texture);
    if (tie1_should_exist)
        tieOne.tie_fighter();
    TieClass tieTwo(Position2[0], Position2[1], Position2[2], 0, 0, -zh, 0.2, tie_texture, wing_texture);
    if (tie2_should_exist)
        tieTwo.tie_fighter();
    TieClass tieThree(Position3[0], Position3[1], Position3[2], -zh, 0, 0, 0.2, tie_texture, wing_texture);
    if (tie3_should_exist)
        tieThree.tie_fighter();

    TieClass pilotTie(Ox, Oy, Oz, ph, rho, th, 0.2, tie_texture, wing_texture);
    if (pilot_tie_should_exist)
        pilotTie.tie_fighter();
    DeathStar deathStar(13, 0, 13, 5, death_star_texture);

    // Attempt to load an awing
    ObjectModel awingModel(Position2[0], Position2[1] + 10, Position2[2], 0, 0, zh, 0.3, 0.3, awing, a_tex[0], a_tex[1]);
    if(awing_should_exist)
        awingModel.draw_model();
//
//    // Attempt to load a bwing
    ObjectModel bwingModel(-Position2[0], -Position2[1] - 10, -Position2[2], 0, 0, 180 + zh, 1.3, 1, bwing, b_tex[0], b_tex[1]);
    if(bwing_should_exist)
        bwingModel.draw_model();

    // if death star should exist, draw it.
    if (death_star_should_exist)
        deathStar.draw_deathstar();

    // Draw lasers, and detect collisions between lasers and other objects.
    for (auto &laser:lasers){
        laser.update(50);
//        laser.draw_laser();

        // If we detect a collision with the death star, create a large explosion quad for 5 seconds
        if (death_star_should_exist && laser.bbox.isCollided(deathStar.bbox)){
            death_star_should_exist = false;
            explosions.push_back(TextureQuad(13, 0, 13, ph, th, 5, explosion_texture, false));
            std::cout << "Boom Baby!" << std::endl;
        }

        if (tie1_should_exist && laser.bbox.isCollided(tieOne.bbox)){
            tie1_should_exist = false;
            explosions.push_back(TextureQuad(Position1[0], Position1[1], Position1[2], th, ph, 1, explosion_texture, true));
        }
        if (tie2_should_exist && laser.bbox.isCollided(tieTwo.bbox)){
            tie2_should_exist = false;
            explosions.push_back(TextureQuad(Position2[0], Position2[1], Position2[2], th, ph, 1, explosion_texture, true));
        }
        if (tie3_should_exist && laser.bbox.isCollided(tieThree.bbox)){
            tie3_should_exist = false;
            explosions.push_back(TextureQuad(Position3[0], Position3[1], Position3[2], th, ph, 1, explosion_texture, true));
        }

        if (awing_should_exist && laser.bbox.isCollided(awingModel.bbox)){
            awing_should_exist = false;
            explosions.push_back(TextureQuad(Position2[0], Position2[1] + 10, Position2[2], th, ph, 1, explosion_texture, false));
        }

        if (bwing_should_exist && laser.bbox.isCollided(bwingModel.bbox)){
            bwing_should_exist = false;
            explosions.push_back(TextureQuad(-Position2[0], -Position2[1] - 10, -Position2[2], th, ph, 1, explosion_texture, false));
        }


    }


    // Check if the pilot has collided with any other objects
    if (tie1_should_exist && pilotTie.bbox.isCollided(tieOne.bbox)){
        tie1_should_exist = false;
        explosions.push_back(TextureQuad(Position1[0], Position1[1], Position1[2], th, ph, 1, explosion_texture, true));
    }

    if (tie2_should_exist && pilotTie.bbox.isCollided(tieTwo.bbox)){
        tie2_should_exist = false;
        explosions.push_back(TextureQuad(Position2[0], Position2[1], Position2[2], th, ph, 1, explosion_texture, true));
    }
    if (tie3_should_exist && pilotTie.bbox.isCollided(tieThree.bbox)){
        tie3_should_exist = false;
        explosions.push_back(TextureQuad(Position3[0], Position3[1], Position3[2], th, ph, 1, explosion_texture, true));
    }

    if (death_star_should_exist && pilotTie.bbox.isCollided(deathStar.bbox)){
        death_star_should_exist = false;
        explosions.push_back(TextureQuad(13, 0, 13, ph, th, 5, explosion_texture, false));
        pilot_tie_should_exist = false;
    }

    if (awing_should_exist && pilotTie.bbox.isCollided(awingModel.bbox)){
        awing_should_exist = false;
        explosions.push_back(TextureQuad(Position2[0], Position2[1] + 10, Position2[2], th, ph, 1, explosion_texture, false));
    }

    if (bwing_should_exist && pilotTie.bbox.isCollided(bwingModel.bbox)){
        bwing_should_exist = false;
        explosions.push_back(TextureQuad(-Position2[0], -Position2[1] - 10, -Position2[2], th, ph, 1, explosion_texture, false));
    }




    // Draw explosions
    for(auto &explosion : explosions){
        glUseProgram(explosion_shader);

        explosion.update_quad_timer(50);
        explosion.update_angles(th, ph);
        explosion.draw_quad();
        glUseProgram(0);

    }

    // Draw storm troopers
    for (auto &trooper:storm_troopers){
        glUseProgram(stormtrooper_shader);

        trooper.update_quad_timer(50);
        trooper.update_angles(th, ph);
        trooper.draw_quad();
        glUseProgram(0);

    }


    // Delete old lasers
    if (lasers.front().should_exist())
        lasers.pop_front();

    // Delete old explosions
    if (explosions.front().quad_should_exist()) {
        if (explosions.front().draw_storm_trooper_) {
            explosions.front().update_texture(storm_trooper_texture);
            storm_troopers.push_back(explosions.front());
        }
        explosions.pop_front();
    }

    // Dont delete the storm troopers


    glWindowPos2i(35,35);
    Print("Ship Yaw = %d, Ship Pitch = %d, Ship Speed = %f",
          th, ph, speed);

    //  Render the scene
    glFlush();
    //  Make the rendered scene visible
    glutSwapBuffers();
    ErrCheck("display");

}


/*
 *  GLUT calls this routine when an arrow key is pressed (Modified from ex25)
 */
void special(int key,int x,int y)
{
    // use the arrow keys to change the viewing angle
    if (key == GLUT_KEY_UP)
        ph += 5;
    else if (key == GLUT_KEY_DOWN)
        ph -= 5;
    else if (key == GLUT_KEY_LEFT)
        th -= 5;
    else if (key == GLUT_KEY_RIGHT)
        th += 5;
    //  Keep angles to +/-360 degrees
    th %= 360;
    ph %= 360;
    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}

/*
 *  GLUT calls this routine when a key is pressed
 */
void key(unsigned char ch,int x,int y)
{
    //  Exit on ESC
    if (ch == 27)
        exit(0);
    // Create a new laser object
    if (ch == ' '){
        lasers.push_back(Laser(Ox, Oy, Oz, Dx, Dy, Dz, th, ph));
    }

    if (ch == 'w'){
        speed += 0.2;
    }
    if (ch == 's'){
        speed -= 0.2;
    }
    if (ch == 'd'){
        rho += 5;
    }
    if (ch == 'a'){
        rho -=5;
    }

    //  Reproject
    Project(fov,asp,dim);

    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}


void timer(int toggle)
{

    int move = 1;
    zh = (zh+1)%360;
    //  Animate flight using Lorenz transform
    if (fly)
    {
        //  Lorenz integration parameters
        /* Should be free to remove these */
        double dt = 0.003;
        double s = -1.7;
        double b = 2.66;
        double r = 50;


        //  Old vectors
        double D,Nx,Ny,Nz;
        double Dx0 = Dx;
        double Dy0 = Dy;
        double Dz0 = Dz;
        double Ux0 = Ux;
        double Uy0 = Uy;
        double Uz0 = Uz;

        //  Fix degenerate case
        if (X==0 && Y==0 && Z==0) Y = Z = 20;


        // Given a starting forward vector of [0, 0, 1], rotate about theta degrees about y axis and phi degrees about x axis
        Dy = Sin(ph);
        Dx = -Sin(th) * Cos(ph);
        Dz = Cos(ph) * Cos(th);
        if(pilot_tie_should_exist) {
            Ox += speed * Dx;
            Oy += speed * Dy;
            Oz += speed * Dz;
        }
    }
        //  Static Roll/Pitch/Yaw
    else
    {
        Ex = -2*dim*Sin(th)*Cos(ph);
        Ey = +2*dim        *Sin(ph);
        Ez = +2*dim*Cos(th)*Cos(ph);
        Ox = Oy = Oz = 0;
        X = Y = Z = 0;
        Dx = 1; Dy = 0; Dz = 0;
        Ux = 0; Uy = 1; Uz = 0;
    }
    //  Set timer to go again
    if (move && toggle>=0) glutTimerFunc(50,timer,0);
    //  Tell GLUT it is necessary to redisplay the scene
    glutPostRedisplay();
}




// Taken from ex25 because ex8 didnt work
void reshape(int width,int height)
{
    //  Ratio of the width to the height of the window
    asp = (height>0) ? (double)width/height : 1;
    //  Set the viewport to the entire window
    glViewport(0,0, width,height);
    //  Set projection
    Project(fov,asp,dim);
}
/*
 *  Read text file
 */
char* ReadText(char *file)
{
    int   n;
    char* buffer;
    //  Open file
    FILE* f = fopen(file,"rt");
    if (!f) Fatal("Cannot open text file %s\n",file);
    //  Seek to end to determine size, then rewind
    fseek(f,0,SEEK_END);
    n = ftell(f);
    rewind(f);
    //  Allocate memory for the whole file
    buffer = (char*)malloc(n+1);
    if (!buffer) Fatal("Cannot allocate %d bytes for text file %s\n",n+1,file);
    //  Snarf the file
    if (fread(buffer,n,1,f)!=1) Fatal("Cannot read %d bytes for text file %s\n",n,file);
    buffer[n] = 0;
    //  Close and return
    fclose(f);
    return buffer;
}
/*
 *  Print Shader Log
 */
void PrintShaderLog(int obj,char* file)
{
    int len=0;
    glGetShaderiv(obj,GL_INFO_LOG_LENGTH,&len);
    if (len>1)
    {
        int n=0;
        char* buffer = (char *)malloc(len);
        if (!buffer) Fatal("Cannot allocate %d bytes of text for shader log\n",len);
        glGetShaderInfoLog(obj,len,&n,buffer);
        fprintf(stderr,"%s:\n%s\n",file,buffer);
        free(buffer);
    }
    glGetShaderiv(obj,GL_COMPILE_STATUS,&len);
    if (!len) Fatal("Error compiling %s\n",file);
}
/*
 *  Print Program Log
 */
void PrintProgramLog(int obj)
{
    int len=0;
    glGetProgramiv(obj,GL_INFO_LOG_LENGTH,&len);
    if (len>1)
    {
        int n=0;
        char* buffer = (char *)malloc(len);
        if (!buffer) Fatal("Cannot allocate %d bytes of text for program log\n",len);
        glGetProgramInfoLog(obj,len,&n,buffer);
        fprintf(stderr,"%s\n",buffer);
    }
    glGetProgramiv(obj,GL_LINK_STATUS,&len);
    if (!len) Fatal("Error linking program\n");
}


/*
 *  Create Shader
 */
int CreateShader(GLenum type,char* file)
{
    //  Create the shader
    int shader = glCreateShader(type);
    //  Load source code from file
    char* source = ReadText(file);
    glShaderSource(shader,1,(const char**)&source,NULL);
    free(source);
    //  Compile the shader
    fprintf(stderr,"Compile %s\n",file);
    glCompileShader(shader);
    //  Check for errors
    PrintShaderLog(shader,file);
    //  Return name
    return shader;
}


/*
 *  Create Shader Program
 */
int CreateShaderProg(char* VertFile,char* FragFile)
{
    //  Create program
    int prog = glCreateProgram();
    //  Create and compile vertex shader
    int vert = CreateShader(GL_VERTEX_SHADER  ,VertFile);
    //  Create and compile fragment shader
    int frag = CreateShader(GL_FRAGMENT_SHADER,FragFile);
    //  Attach vertex shader
    glAttachShader(prog,vert);
    //  Attach fragment shader
    glAttachShader(prog,frag);
    //  Link program
    glLinkProgram(prog);
    //  Check for errors
    PrintProgramLog(prog);
    //  Return name
    return prog;
}


/*
 *  Start up GLUT and tell it what to do
 */
// Taken from examples
int main(int argc,char* argv[])
{
    //  Initialize GLUT and process user parameters
    glutInit(&argc,argv);
    //  Request double buffered, true color window with Z buffering at 600x600
    glutInitWindowSize(1000,1000);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    //  Create the window
    glutCreateWindow("Ryan Leonard Project");
    //  Tell GLUT to call "display" when the scene should be drawn
    glutDisplayFunc(display);
    //  Tell GLUT to call "reshape" when the window is resized
    glutReshapeFunc(reshape);
    //  Tell GLUT to call "special" when an arrow key is pressed
    glutSpecialFunc(special);
    //  Tell GLUT to call "key" when a key is pressed
    glutKeyboardFunc(key);
//    glutIdleFunc(idle);

  /*  // Absolute path for Clion
    tie_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/tie_texture_2.bmp");
    wing_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/pinstripe_small.bmp");
    death_star_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Homework3/death_star_alt.bmp");
    explosion_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/explosion1_test.bmp");
//    storm_trooper_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/storm_trooper2.bmp");
    storm_trooper_texture = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/storm_trooper2_red.bmp");
    sky[0] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/sky0.bmp");
    sky[1] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/sky1.bmp");
    spaceland = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/spaceland_small.bmp");
    plaid = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/plaid.bmp");
    recruitment = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/recruitment2.bmp");

    // Load an A-Wing
    a_tex[0] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/A_TEX_1.bmp");
    a_tex[1] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/TEX_2.bmp");
    awing = LoadOBJ("/home/ryan/git-repos/glut_csci5229/Project/reversed_normals_A-wingExterior.obj");

    // Load a B-Wing
    b_tex[0] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/B_TEX_1.bmp");
    b_tex[1] = LoadTexBMP("/home/ryan/git-repos/glut_csci5229/Project/B_TEX_1.bmp");
    bwing = LoadOBJ("/home/ryan/git-repos/glut_csci5229/Project/reversed_normals_B-wingExterior.obj");

    stormtrooper_shader = CreateShaderProg("/home/ryan/git-repos/glut_csci5229/Project/texture.vert","/home/ryan/git-repos/glut_csci5229/Project/stormtrooper.frag");
    explosion_shader = CreateShaderProg("/home/ryan/git-repos/glut_csci5229/Project/texture.vert","/home/ryan/git-repos/glut_csci5229/Project/texture.frag");

    ErrCheck("main");
*/

    // Relative Path for submission
    tie_texture = LoadTexBMP("tie_texture_2.bmp");
    wing_texture = LoadTexBMP("pinstripe_small.bmp");
    death_star_texture = LoadTexBMP("death_star_alt.bmp");
    explosion_texture = LoadTexBMP("explosion1_test.bmp");
    storm_trooper_texture = LoadTexBMP("storm_trooper2_red.bmp");
//    sky[0] = LoadTexBMP("sky0.bmp");
//    sky[1] = LoadTexBMP("sky1.bmp");
    spaceland = LoadTexBMP("spaceland_small.bmp");
    plaid = LoadTexBMP("plaid.bmp");
    recruitment = LoadTexBMP("recruitment2.bmp");
        // Load an A-Wing
    a_tex[0] = LoadTexBMP("A_TEX_1.bmp");
    a_tex[1] = LoadTexBMP("A_TEX_1.bmp");
    awing = LoadOBJ("reversed_normals_A-wingExterior.obj");

    // Load a B-Wing
    b_tex[0] = LoadTexBMP("B_TEX_1.bmp");
    b_tex[1] = LoadTexBMP("B_TEX_1.bmp");
    bwing = LoadOBJ("reversed_normals_B-wingExterior.obj");

    stormtrooper_shader = CreateShaderProg("texture.vert","stormtrooper.frag");
    explosion_shader = CreateShaderProg("texture.vert","texture.frag");

    ErrCheck("main");

    timer(1);

    //  Pass control to GLUT so it can interact with the user
    glutMainLoop();
    return 0;
}

//
// Created by ryan on 7/2/18.
//

#ifndef PATENT_INFRINGEMENT_TIMEBOMB_EXPLOSION_H
#define PATENT_INFRINGEMENT_TIMEBOMB_EXPLOSION_H

#include <math.h>
#include <GL/glut.h>
#include <iostream>

//  Cosine and Sine in degrees
#define Cos(x) (cos((x)*3.1415927/180))
#define Sin(x) (sin((x)*3.1415927/180))


class TextureQuad {
public:
    // TODO: Create a quad that is automatically scaled to the size of the object.
    // Constructor
    TextureQuad(double x, double y, double z, double th, double ph, double size,
              unsigned int texture, bool draw_storm_trooper) :
            x_(x),
            y_(y),
            z_(z),
            th_(th),
            ph_(ph),
            size_(size),
            texture_(texture),
            draw_storm_trooper_(draw_storm_trooper)
    {
    }

    // Destructor
    ~TextureQuad(){}

    void draw_quad();

    void update_quad_timer(double time);

    bool quad_should_exist();

    void update_angles(double th, double ph);
    // TODO: Load a storm trooper or series of storm troopers to replace explosion after a time limit

    void update_texture(unsigned int texture){
        this->texture_ = texture;
    }
    bool draw_storm_trooper_;

private:
    double x_, y_, z_, th_, ph_, size_;
    unsigned int texture_;
    double quad_timer = 0;
    int rotation = 0;

};


#endif //PATENT_INFRINGEMENT_TIMEBOMB_EXPLOSION_H

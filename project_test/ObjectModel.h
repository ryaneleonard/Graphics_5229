//
// Created by ryan on 7/3/18.
//

#ifndef PATENT_INFRINGEMENT_TIMEBOMB_BLENDERMODEL_H
#define PATENT_INFRINGEMENT_TIMEBOMB_BLENDERMODEL_H

#include <math.h>
#include <GL/glut.h>
#include <iostream>
#include "CSCIx229.h"
#include "AABB.h"

class ObjectModel {
public:
    ObjectModel(double x, double y, double z, double th, double rho, double ph, double scale, double bbox_scale, int object, unsigned int texture1, unsigned int texture2):
    x_(x),
    y_(y),
    z_(z),
    th_(th),
    rho_(rho),
    ph_(ph),
    scale_(scale),
    object_(object),
    texture1_(texture1),
    texture2_(texture2)
    {
        this->bbox.initialize(this->x_, this->y_, this->z_, bbox_scale * 5, bbox_scale * 5, bbox_scale * 5);
    }

    ~ObjectModel(){}

    void draw_model();

    AABB bbox;
private:
    double x_, y_, z_, th_, rho_, ph_, scale_;
    int object_;
    unsigned int texture1_, texture2_;
};


#endif //PATENT_INFRINGEMENT_TIMEBOMB_BLENDERMODEL_H
